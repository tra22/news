package news.project.social.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class NewsController {

        @GetMapping("/category")
        public String category( ){

            return "admin/category";
        }

        @GetMapping("/dashboard")
        public String dashboad( ){
            return "/admin/dashboard";
        }

        @GetMapping("/content")
        public String content( ){
            return "/admin/content";
        }


}
