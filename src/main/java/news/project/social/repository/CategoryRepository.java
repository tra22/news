package news.project.social.repository;

import news.project.social.model.Category;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository {

    @Select("Select * from tblcategory")
    public Category category();
}
