package news.project.social.model;

public class Image {
    public String imgId;
    public Content contentId;
    public String imgTitle;
    public String imgPath;
    public Integer imgOrder;

    public Image() {
    }

    @Override
    public String toString() {
        return "Image{" +
                "imgId='" + imgId + '\'' +
                ", contentId=" + contentId +
                ", imgTitle='" + imgTitle + '\'' +
                ", imgPath='" + imgPath + '\'' +
                ", imgOrder=" + imgOrder +
                '}';
    }

    public String getImgId() {
        return imgId;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }

    public Content getContentId() {
        return contentId;
    }

    public void setContentId(Content contentId) {
        this.contentId = contentId;
    }

    public String getImgTitle() {
        return imgTitle;
    }

    public void setImgTitle(String imgTitle) {
        this.imgTitle = imgTitle;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public Integer getImgOrder() {
        return imgOrder;
    }

    public void setImgOrder(Integer imgOrder) {
        this.imgOrder = imgOrder;
    }
}
