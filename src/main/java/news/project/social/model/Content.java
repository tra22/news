package news.project.social.model;

public class Content {
    public String conId;
    public Category catId;
    public Register regId;
    public String title;
    public String content;
    public String publishDate;
    public Integer priority;
    public Integer popular;
    public String createDate;

    public Content() {
    }

    @Override
    public String toString() {
        return "Content{" +
                "conId='" + conId + '\'' +
                ", catId=" + catId +
                ", regId=" + regId +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", publishDate='" + publishDate + '\'' +
                ", priority=" + priority +
                ", popular=" + popular +
                ", createDate='" + createDate + '\'' +
                '}';
    }

    public String getConId() {
        return conId;
    }

    public void setConId(String conId) {
        this.conId = conId;
    }

    public Category getCatId() {
        return catId;
    }

    public void setCatId(Category catId) {
        this.catId = catId;
    }

    public Register getRegId() {
        return regId;
    }

    public void setRegId(Register regId) {
        this.regId = regId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getPopular() {
        return popular;
    }

    public void setPopular(Integer popular) {
        this.popular = popular;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
