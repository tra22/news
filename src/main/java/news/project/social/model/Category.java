package news.project.social.model;

public class Category {
    public Integer catId;
    public Register regId;
    public String name;
    public Integer parentId;
    public String createDate;

    public Category() {
    }

    @Override
    public String toString() {
        return "Category{" +
                "catId=" + catId +
                ", regId=" + regId +
                ", name='" + name + '\'' +
                ", parentId=" + parentId +
                ", createDate='" + createDate + '\'' +
                '}';
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Register getRegId() {
        return regId;
    }

    public void setRegId(Register regId) {
        this.regId = regId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
